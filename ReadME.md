# 爬取博碩士論文網頁資料

## 使用方式

+ 1. 下載專案內會用到的套件
```javascript
pip install selenium
```

+ 2. 安裝WebDriver 
+ 學習資源 [Youtube] https://www.youtube.com/watch?v=ximjGyZ93YQ
+ 下載WebDriver [WebDriver] https://sites.google.com/chromium.org/driver/downloads?authuser=0

+ 3. 目前只能修改學校跟科系可以看getPaper.py檔案的第43行那邊有給範例~

## Python 協作規範:

### 前導：

尚未更新

<br>  

*本專案之規範任然可視情況一同協議更好的規範*

#### 注意事項：

## 推版規則
1. 推版規則：
    1. Master做主要的分支，協作者須另開新分支最後在合併到master
    2. 每次要推版前，都先抓最新的版本下來（共用元件），再推版上去
    3. 參照目錄結構

## Git comment 規範
狀態分為以下幾種：
1.  feat: 新增/修改功能 (feature)。
2.  del: 移除功能或檔案。
3.  fix: 修補 bug (bug fix)。
4.  docs: 文件 (documentation)。
5.  style: 格式 (不影響程式碼運行的變動 white-space, formatting, missing semi colons, etc)。
6.  refactor: 重構 (既不是新增功能，也不是修補 bug 的程式碼變動)。
7.  perf: 改善效能 (A code change that improves performance)。
8.  test: 增加測試 (when adding missing tests)。
9.  chore: 建構程序或輔助工具的變動 (maintain)。
10. revert: 撤銷回覆先前的 commit 例如：revert: type(scope): subject (回覆版本：xxxx)。
11. tmp: 暫存，用於不同電腦間同步未完成的程式碼時使用。

git comment 內容格式如下：

「狀態」（必須，從上述狀態選擇最符合的狀態）：「主旨」（必須，此comment的簡短描述，不要超過50字，結尾不句號）

##分支說明
master: 正式機用的分支,自動部署也是用這個分支

develop: 本地開發測試用的分支
