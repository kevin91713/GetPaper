import time
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import Select
import csv

# 總共有幾頁
page = 0
# 每頁有幾筆資料
item = 100

def replaceTitle(text):
    items = []

    for data in text.split('／'):
        items.append(data)

    num = 2
    for item in items[2].split('\n'):
        if num == 2:
            items[num] = item
            num += 1
        else:
            replaceItem = item.replace(' ','').replace('國圖紙本論文','').replace('被引用:','').replace('點閱:','').replace('評分','').replace('下載:','').replace('書目收藏:','').replace('電子全文','').replace('(本篇限研究生所屬學校校內系統及IP範圍內開放)','')
            if '(網際網路公開日期：' in replaceItem:
                replaceItem = replaceItem[:replaceItem.index('(網際網路公開日期：') + 9]
                replaceItem = replaceItem.replace('(網際網路公開日期','')
            if replaceItem.strip() != '':
                items.append(replaceItem if replaceItem.strip() != '' else '')
    return items

# 國防大學爬法
def NduMis():
    # print("您現在正在爬 國防大學 資訊管理學系 所的論文資料")
    time.sleep(3)
    # ("國防大學".asc and "資訊管理學".sdp)
    search = driver.find_element(By.ID, "ysearchinput0")
    # search.send_keys("(\"國防大學\".asc and \"資訊管理學\".sdp) ")
    # search.send_keys("(\"國立臺灣科技大學\".asc and \"資訊工程\".sdp) ")
    search.send_keys("(\"國立臺灣科技大學\".asc and \"資訊工程\".sdp) ")
    # 搜尋按鈕
    searchButton = driver.find_element(By.ID, "gs32search")
    searchButton.click()
    # 這邊要做排序畢業學年度，所以要等到下拉選單載入完成才可以繼續下一步
    sortby = Select(WebDriverWait(driver, 20).until(
        EC.presence_of_element_located((By.ID, "sortby"))
    ))
    sortby.select_by_value("yr")
    # 要先做好遞增排序後才可以把條列改成表格式
    searchButton = Select(driver.find_element(By.ID, "selchgfmt"))
    searchButton.select_by_index(1)
    time.sleep(1)
    searchButton.select_by_index(0)
    # 記錄每頁的筆數晚點要做迴圈比較方便
    # searchButton = Select(driver.find_element(By.ID, "jpsize"))
    # searchButton.select_by_value("10")
    time.sleep(5)
    # searchButton.select_by_value("100")
    # 將總共有的頁數記錄下來晚點做迴圈使用
    num = WebDriverWait(driver, 20).until(
        EC.presence_of_element_located((By.XPATH,
                                        '//*[@id="bodyid"]/form/div/table/tbody/tr[1]/td[2]/table/tbody/tr[4]/td/div[1]/table/tbody/tr[4]/td/div/table/tbody/tr/td[6]'))
    )
    page = int(num.text.replace('/', '').replace('頁', ''))
    #開始跑每一頁
    for nowPage in range(0,page,1):
        with open('paper.csv', 'a', newline='', encoding='utf-8-sig') as csvfile:
            # 可以開始取得所有資料了
            table = WebDriverWait(driver, 20).until(
                EC.presence_of_element_located((By.ID, "tablefmt1"))
            )
            trlist = table.find_elements(By.TAG_NAME, 'td')
            s = []
            index = 0
            writer = csv.writer(csvfile)
            writer.writerow(
                ['需要刪除', '編號', '論文題目', '作者姓名', '指導教授', '學校', '系所名稱', '畢業學年度', '被引用次數',
                 '點閱次數', '評分', '下載次數', '書目收藏'])
            for row in trlist:
                if (index == 5):
                    newtext = replaceTitle(row.text)
                    for i in newtext:
                        s.append(i)
                    writer.writerow(s)
                    s = []
                    index = 0
                else:
                    s.append(row.text)
                    index += 1
            if nowPage+1 < page:
                #換下一頁
                nextPage = driver.find_element(By.NAME,"gonext")
                nextPage.click()
            print("第 " + str(nowPage+1) + " 頁執行完成!")


path = "D:\\Users\\User\\Downloads\\chromedriver_win32\\chromedriver.exe"
driver = webdriver.Chrome(path)
# 進入網址
driver.get("https://ndltd.ncl.edu.tw/cgi-bin/gs32/gsweb.cgi/ccd=.oCbVX/search?mode=advance")
# 進入進階查詢方便爬
link = WebDriverWait(driver, 10).until(
    EC.presence_of_element_located((By.LINK_TEXT, "指令查詢"))
)
link.click()
# 以下開始搜尋
NduMis()

time.sleep(5)
driver.quit()

